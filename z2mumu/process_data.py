#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =============================================================================
# @file   process_data.py
# @author Albert Puig (albert.puig@cern.ch)
# @date   23.11.2017
# =============================================================================
"""Process data to prepare weights and clean up."""

import os

import pandas as pd

from analysis.data.hdf import modify_hdf
from analysis.data.writers import write_dataset

TEST = True

VARLIST = ['THETA_MU_PLUS_Z_FRAME',
           'PHI_MU_PLUS_Z_FRAME',
           'WEIGHTS_PHI_ACC_MU_PLUS',
           'WEIGHTS_THETA_ACC_MU_PLUS']


if __name__ == "__main__":
    with pd.HDFStore('dfDataZ0MagDown_ext.h5') as input_file:
        df = input_file['table']
        new_table = df[VARLIST]
    new_table = new_table.assign(ACC_PHI_INV=lambda x: 1.0/x['WEIGHTS_PHI_ACC_MU_PLUS'],
                                 ACC_THETA_INV=lambda x: 1.0/x['WEIGHTS_THETA_ACC_MU_PLUS']).dropna()
    if os.path.exists('Data_processed.h5'):
        os.remove('Data_processed.h5')
    with modify_hdf('Data_processed.h5') as output_file:
        output_file.append('table', new_table)
    if TEST:
        write_dataset('Data_processed.root', new_table)

# EOF
