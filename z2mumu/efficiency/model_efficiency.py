#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =============================================================================
# @file   model_efficiency.py
# @author Albert Puig (albert.puig@cern.ch)
# @date   02.05.2017
# =============================================================================
"""Model efficiency."""

from analysis.efficiency.model_efficiency import main
from z2mumu import init

# Configure the module
init()

if __name__ == "__main__":
    main()

# EOF
