#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =============================================================================
# @file   fit_data.py
# @author Albert Puig (albert.puig@cern.ch)
# @date   16.11.2017
# =============================================================================
"""Fit the data."""

from math import pi

from analysis.data import get_data
from analysis.physics import get_physics_factory
from analysis.fit import fit
from analysis.utils.logging_color import get_logger

import ROOT

from z2mumu import init

# Set verbose output
get_logger('analysis', 10)

init()


if __name__ == "__main__":
    dataset = get_data({'source': 'Data_processed.h5',
                        'tree': 'table',
                        'output-format': 'root'},
                       **{'variables': ['THETA_MU_PLUS_Z_FRAME',
                                        'PHI_MU_PLUS_Z_FRAME',
                                        'WEIGHTS_PHI_ACC_MU_PLUS',
                                        'WEIGHTS_THETA_ACC_MU_PLUS'],
                          'weights-to-normalize': ['ACC_PHI_INV', 'ACC_THETA_INV'],
                          'name': 'data',
                          'title': 'data',
                          'ranges': {'THETA_MU_PLUS_Z_FRAME': [0.0, pi],
                                     'PHI_MU_PLUS_Z_FRAME': [-pi, pi]}})

    pdf_factory = get_physics_factory('angular', 'signal')({'parameters': {'A0': 'const 0.0',
                                                                           'A1': 'const 0.0',
                                                                           'A2': 'const 0.0',
                                                                           'A3': 'const 0.0',
                                                                           'A4': 'const 0.0',
                                                                           'A5': 'const 0.0',
                                                                           'A6': 'const 0.0',
                                                                           'A7': 'const 0.0'},
                                                            'observable-names': {'theta': 'THETA_MU_PLUS_Z_FRAME',
                                                                                 'phi': 'PHI_MU_PLUS_Z_FRAME'}})
    # pdf_factory = get_physics_factory('angular', 'signal')({'parameters': {'A0': 'var 0.0 0.0 1.0',
    #                                                                        'A1': 'var 0.0 0.0 1.0',
    #                                                                        'A2': 'var 0.0 0.0 1.0',
    #                                                                        'A3': 'var 0.0 0.0 1.0',
    #                                                                        'A4': 'var 0.0 0.0 1.0',
    #                                                                        'A5': 'var 0.0 0.0 1.0',
    #                                                                        'A6': 'var 0.0 0.0 1.0',
    #                                                                        'A7': 'var 0.0 0.0 1.0'},
    #                                                         'observable-names': {'theta': 'THETA_MU_PLUS_Z_FRAME',
    #                                                                              'phi': 'PHI_MU_PLUS_Z_FRAME'}})
    fit_result = fit(pdf_factory, 'AngularPdf', 'simple', dataset, verbose=True, Minos=False, SumW2Error=True)
    frame = dataset.get()['THETA_MU_PLUS_Z_FRAME'].frame(10)
    dataset.plotOn(frame, ROOT.RooFit.DataError(ROOT.RooAbsData.SumW2))
    pdf_factory.get_pdf("AngularPdf", "AngularPdf").plotOn(frame)
    frame.Draw()

# EOF
