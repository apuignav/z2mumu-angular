#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =============================================================================
# @file   produce_gen_level.py
# @author Albert Puig (albert.puig@cern.ch)
# @date   29.09.2017
# =============================================================================
"""Produce generator level MC."""

from analysis.mc.produce_gen_level import main
from z2mumu import init

# Configure the module
init(True)

if __name__ == "__main__":
    main()

# EOF
