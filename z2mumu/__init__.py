#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =============================================================================
# @file   __init__.py
# @author Albert Puig (albert.puig@cern.ch)
# @date   02.05.2017
# =============================================================================
"""Z->mumu angular analysis."""


def init():
    """Setup the z2mumu module.

    Sets the base path and loads the physics factories.

    """
    import os

    from analysis import set_global_var
    from analysis.physics import register_physics_factories
    from analysis.fit import register_fit_strategy
    from analysis.utils.logging_color import get_logger
    from analysis.utils.pdf import add_pdf_paths

    from z2mumu.physics import _FACTORIES

    # Logging
    _logger = get_logger('z2mumu')
    # Set the new base path
    base_path = os.path.abspath(os.path.join(os.path.dirname(__file__)))
    _logger.info("Setting base path to %s", base_path)
    set_global_var('BASE_PATH', base_path)
    # Set factories
    if _FACTORIES:
        _logger.info("Setting factories for %s observables", ', '.join(_FACTORIES.keys()))
        for observable, factories in _FACTORIES.items():
            register_physics_factories(observable, factories)
    # PDF path search example
    pdf_paths = add_pdf_paths('pdfs')
    _logger.info("Searching for PDFs in %s", pdf_paths)


# EOF
