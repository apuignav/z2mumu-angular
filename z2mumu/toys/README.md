Toys
====

Scripts for toy data generation and fitting.
These simply import their counterparts in `analysis` (please find the detailed `README` there) and initialize `z2mumu` before executing the `main` function.

