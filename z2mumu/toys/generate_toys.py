#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =============================================================================
# @file   generate_toys.py
# @author Albert Puig (albert.puig@cern.ch)
# @date   02.05.2017
# =============================================================================
"""Toy generation."""

from analysis.toys.generate_toys import main
from z2mumu import init

# Configure the module
init()


if __name__ == "__main__":
    main()

# EOF
