#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =============================================================================
# @file   submit_toys.py
# @author Albert Puig (albert.puig@cern.ch)
# @date   02.05.2017
# =============================================================================
"""Submission of `fit_toys.py` to the cluster.

This submission is forced to happen through a config file in YAML format
to ensure reproducibility.

Mandatory configuration keys are:
    - name: Name of the job

Then, according to the type of job, there's a few extra mandatory keys:
    - Fitting toys:
        + fit/nfits: Total number of fits to perform.
        + fit/nfits-per-job: Number of fits produced per job.
    - Generation toys:
        + gen/nevents: Total number of events to produce.
        + gen/nevents-per-job: Number of events produced per job.

Optional configuration keys:
    - batch/runtime: In the HH:MM:SS format. Defaults to 08:00:00.

"""

from analysis.toys.submit_toys import main
from z2mumu import init

# Configure the module
init()


if __name__ == "__main__":
    main()

# EOF
