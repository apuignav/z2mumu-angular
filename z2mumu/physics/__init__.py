#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =============================================================================
# @file   __init__.py
# @author Albert Puig (albert.puig@cern.ch)
# @date   02.05.2017
# =============================================================================
"""Physics models.

Prepare the list of factories so the package can be setup.

"""

from .angular import FACTORIES as angular_factories


_FACTORIES = {'angular': angular_factories}

# EOF
