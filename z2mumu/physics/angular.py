#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =============================================================================
# @file   angular.py
# @author Albert Puig (albert.puig@cern.ch)
# @date   02.05.2017
# =============================================================================
"""Angular observables."""

from math import pi

import ROOT

from analysis.utils.pdf import load_pdf_by_name
from analysis.physics.factory import PhysicsFactory
from analysis.utils.logging_color import get_logger


logger = get_logger('z2mumu.physics.angular')


# pylint: disable=W0223
class AngularFactory(PhysicsFactory):
    """Abstract base class for angular analysis."""

    OBSERVABLES = (("theta", "#theta", 0.0, pi, ""),
                   ("phi", "#phi", -pi, pi, ""))


# Factories
class SignalFactory(AngularFactory):
    """Angular analysis object factory.

    Parameter names:
        - 'A0' to 'A7'

    No defaults are given.

    """

    PARAMETERS = ('A0',
                  'A1',
                  'A2',
                  'A3',
                  'A4',
                  'A5',
                  'A6',
                  'A7')

    def get_unbound_pdf(self, name, title):
        """Get the angular PDF corresponding to the given folding.

        Return:
            `ROOT.RooAbsPdf`: Angular PDF.

        """
        return load_pdf_by_name('Z2MuMuAngular')(name, title,
                                                 *(self.get_observables() + self.get_fit_parameters()))


FACTORIES = {'signal': SignalFactory}

# EOF
