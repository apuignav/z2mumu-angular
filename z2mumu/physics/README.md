Physics
=======

Classes defining angular, mass and $q^2$ factories, used to generate PDFs.

For each type of observable, that is `angular`, `mass` or `q2`, a PDF can be defined, in the most general case, with a YAML such as:

```yaml
obs_type:
   pdf: pdf_type
   parameters:
       param1_name: param1_value
       param2_name: param2_value
```

Where `obs_type` can be `angular`, `mass` or `q2` and `pdf_type` depends on each observable type (see each module documentation for the defined PDFs).


Additional configuration
------------------------

Each particular observable type has extra configuration keys that can be added in the dictionary (see each module's documentation for more details).
In a nutshell, these are:

  - Angular: `folding` to be applied.
  - Mass: `mass_limits` defined as a dictionary `{'high': 5200, 'low': 5400}`.


