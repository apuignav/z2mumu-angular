#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =============================================================================
# @file   test_pdfs.py
# @author Albert Puig (albert.puig@cern.ch)
# @date   02.05.2017
# =============================================================================
"""Test that PDFs still work."""

import yaml
import yamlordereddictloader
import pytest

import ROOT

import analysis.physics as phys
import analysis.physics.factory as phys_factory
from analysis.utils.logging_color import get_logger

from z2mumu import init
from z2mumu.physics.angular import SignalFactory


get_logger('analysis.physics').setLevel(10)
get_logger('z2mumu.physics').setLevel(10)


init()


# A helper
def load_model(config_str):
    """Load a model using `configure_model`."""
    factory_config = yaml.load(config_str,
                               Loader=yamlordereddictloader.Loader)
    return phys.configure_model(factory_config)


@pytest.fixture
def angular_factory():
    """Load a PhysicsFactory."""
    return load_model("""angular:
    pdf: signal
    parameters:
        A0: 0.0
        A1: 0.0
        A2: 0.0
        A3: 0.0
        A4: 0.0
        A5: 0.0
        A6: 0.0
        A7: 0.0
""")


# pylint: disable=W0621
def test_factory_load(angular_factory):
    """Test factory loading returns an object of the correct type."""
    return all((isinstance(angular_factory, phys_factory.PhysicsFactory),
                isinstance(angular_factory, SignalFactory)))


def test_pdf_load(angular_factory):
    """Test the PDF creation."""
    pdf = angular_factory.get_pdf("AngularTest", "AngularTest")
    return isinstance(pdf, ROOT.RooAbsPdf)

# EOF
