z2mumu analysis module
======================

Install
-------

To make it work, do

```
pip install -e .
```

The only requirement is analysis tools.
If `pip install` fails, run first:

```
pip install https://gitlab.cern.ch/apuignav/analysis-tools.git
```

Initialization
--------------

The `z2mumu/__init__.py` file implements an `init` function that has to be called in each script in order to load the proper paths and physics factories for the analysis.


