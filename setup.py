#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =============================================================================
# @file   setup.py
# @author Albert Puig (albert.puig@cern.ch)
# @date   02.05.2017
# =============================================================================
"""B->K*ee analysis package."""

from setuptools import setup
setup(name='z2mumu',
      version='0.1',
      description='Z->mumu angular analysis',
      url='https://gitlab.cern.ch/apuignav/z2mumu-angular',
      author='Albert Puig',
      author_email='albert.puig@cern.ch',
      license='BSD3',
      install_requires=['analysis'],
      dependency_links=['https://gitlab.cern.ch/apuignav/analysis-tools.git/tarball/master#egg=analysis-1.0'],
      packages=['z2mumu'],
      zip_safe=False)

# EOF
